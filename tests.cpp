//
// Created by araujvin on 15/05/22.
//

#include "tests.h"
#include "ITAndroidsCalculator.h"

using namespace std;

/// FUNCTION 1
bool testAdd(){
    if((add(21.0, 21.0) - 42.0) <= 0.00001)
        return true;

    cout << "Add function failed"<< endl;
    return false;
}

/// FUNCTION 2
bool testSubtract(){
    if((subtract(24.0, 21.0) - 3.0) <= 0.00001)
        return true;

    cout << "Subtract function failed." << endl;
    return false;
}

/// FUNCTION 3
bool testMultiply(){
    if((multiply(21.0, 21.0) - 441.0) <= 0.00001)
        return true;

    cout << "Multiply function failed." << endl;
    return false;
}

/// FUNCTION 4
bool testDivide(){
    if((divide(21.0, 2.0) - 10.5) <= 0.00001)
        return true;

    cout << "Divide function failed." << endl;
    return false;
}

/// FUNCTION 5
bool testSquare(){
    if((square(21.0) - 441.0) <= 0.00001)
        return true;

    cout << "Square function failed." << endl;
    return false;
}

/// FUNCTION 6
bool testCube(){
    if((cube(21.0) - 9261.0) <= 0.00001)
        return true;

    cout << "Cube function failed." << endl;
    return false;
}

/// FUNCTION 7
bool testRaise(){
    if((raise(21.0, 4.0) - 194481.0) <= 0.00001)
        return true;

    cout << "Raise function failed." << endl;
    return false;
}

/// FUNCTION 8
bool testSquareRoot(){
    if((squareRoot(121.0) - 11.0) <= 0.00001)
        return true;

    cout << "Square root function failed." << endl;
    return false;
}

/// FUNCTION 9
bool testCubicRoot(){
    if((cubicRoot(9261.0) - 21.0) <= 0.00001)
        return true;

    cout  << "Cubic root function failed.\n";
    return false;
}

/// FUNCTION 10
bool testNthRoot(){
    if((nthRoot(194481.0, 4) - 21.0) <= 0.00001)
        return true;

    cout << "Nth root function failed.";
    return false;
}

/// FUNCTION 11
bool testBase10Logarithm(){
    if((base10Logarithm(1000.0) - 3.0) <= 0.00001)
        return true;

    cout << "Base-10 logarithm function failed." << endl;
    return false;
}

/// FUNCTION 12
bool testNaturalLogarithm(){
    if((naturalLogarithm(2.718281828459045235360287) - 1.0) <= 0.00001)
        return true;

    printf("Natural logarithm function failed.\n");
    return false;
}

/// FUNCTION 13
bool testEulerConstant(){
    if((eulerConstant(2) - 2.72) <= 0.00001)
        return true;

    printf("Euler constant function failed.\n");
    return false;
}

/// FUNCTION 14
bool testBaseNLogarithm(){
    if((baseNLogarithm(8.0, 2) - 3.0) <= 0.00001)
        return true;

    printf("Base-n logarithm function failed.\n");
    return false;
}

/// FUNCTION 15
bool testCosine(){
    if((cosine(1.047197551) - 0.5) <= 0.00001)
        return true;

    printf("Cosine function failed.\n");
    return false;
}

/// FUNCTION 16
bool testSine(){
    if((sine(0.523598776) - 0.5) <= 0.00001)
        return true;

    printf("Sine function failed.\n");
    return false;
}

/// FUNCTION 17
bool testTangent(){
    if((tangent(0.785398163) - 1.0) <= 0.00001)
        return true;

    printf("Tangent function failed.\n");
    return false;
}

/// FUNCTION 18
bool testPi(){
    if((pi(2) - 3.14) <= 0.00001)
        return true;

    printf("Pi function failed.\n");
    return false;
}

/// FUNCTION 19
bool testDegreesToRadians(){
    if((degreesToRadians(180) - 3.14159265359) <= 0.00001)
        return true;

    printf("Degrees to radians function failed.\n");
    return false;
}
/// FUNCTION 20
bool testArcCosine(){
    if((arcCosine(0.5) - 1.047197551) <= 0.00001)
        return true;

    printf("Arc cosine function failed.\n");
    return false;
}

/// FUNCTION 21
bool testArcSine(){
    if((arcSine(0.5) - 0.523598776) <= 0.00001)
        return true;

    cout << "Arc sine function failed." << endl;
    return false;
}

/// FUNCTION 22
bool testArcTangent(){
    if((arcTangent(1.0) - 0.785398163) <= 0.00001)
        return true;

    cout << "Arc tangent function failed." << endl;
    return false;
}

/// FUNCTION 23
bool testChangeSign(){
    if((changeSign(-21.0) - 21.0) <= 0.00001)
        return true;

    cout << "Change sign function failed." << endl;
    return false;
}

/// FUNCTION 24
bool testAbsoluteValue(){
    if((absoluteValue(-21.0) - 21.0) <= 0.00001)
        return true;

    cout << "Absolute value function failed." << endl;
    return false;
}

/// FUNCTION 25
bool testInvert(){
    if((invert(4.0) - 0.25) <= 0.00001)
        return true;

    cout << "Invert function failed." << endl;
    return false;
}

/// FUNCTION 26
bool testPercentage(){
    if((percentage(21.0) - 0.21) <= 0.00001)
        return true;

    cout << "Factorial function failed." << endl;
    return false;
}

/// FUNCTION 27
bool testFactorial(){
    if((factorial(4.0) - 24.0) <= 0.00001)
        return true;

    cout << "Factorial function failed." << endl;
    return false;
}

/// FUNCTION 28
bool testPowerOf10(){
    if(powerOf10(4) == 10000.0)
        return true;

    cout << "Power of 10 function failed." << endl;
    return false;
}

/// FUNCTION 29
bool testHyperbolicCosine(){
    if((hyperbolicCosine(0.0) - 1.0) <= 0.00001)
        return true;

    cout << "Hyperbolic cosine function failed." << endl;
    return false;
}

/// FUNCTION 30
bool testHyperbolicSine(){
    if((hyperbolicSine(0.0) - 0.0) <= 0.00001)
        return true;

    cout << "Hyperbolic sine function failed." << endl;
    return false;
}

/// FUNCTION 31
bool testHyperbolicTangent(){
    if((hyperbolicTangent(0.0) - 0.0) <= 0.00001)
        return true;

    cout << "Hyperbolic sine function failed." << endl;
    return false;
}

/// FUNCTION 32
bool testClosestInteger(){
    if(closestInteger(21.4) == 21)
        return true;

    cout << "Closest integer function failed." << endl;
    return false;
}

/// FUNCTION 33
bool testMax(){
    if((max(21.0, 20.0) - 21.0) <= 0.00001)
        return true;

    cout << "Max function failed." << endl;
    return false;
}


void runAllTests(){
    if( testAdd() && testSubtract() && testMultiply() && testDivide() && testSquare() && testCube() &&
        testRaise() && testSquareRoot() && testCubicRoot() && testNthRoot() && testBase10Logarithm() &&
        testNaturalLogarithm() && testEulerConstant() && testBaseNLogarithm() && testCosine() &&
        testSine() && testTangent() && testPi() && testDegreesToRadians() && testArcCosine() &&
        testArcSine() && testArcTangent() && testChangeSign() && testAbsoluteValue() && testInvert() &&
        testPercentage() && testFactorial() && testPowerOf10() && testHyperbolicCosine() &&
        testHyperbolicSine() && testHyperbolicTangent() && testClosestInteger() && testMax())
        cout << "All tests passed" << endl;

}
